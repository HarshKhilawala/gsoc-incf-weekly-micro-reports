.. _week1:


Week 1 Microreport (June 7 - June 13):
=======================================

What did I do this week?
-------------------------
Get to know more about the about the project interacting with the mentor, role of Statistics in it and how Statistics is different from what it seems to normal people (People usually think it's all about numbers but it's not!).
Also discussed some possible way to make progress with the project.

What are the plans for next week?
--------------------------------------
For the next week, Plan is not to have approach to go too fast with the project. Top priority would be iterating the validation tests multiple times over different possible sets of proxy/hypothetical data.


Am I blocked on anything?
--------------------------
No, I am not blocked upon anything yet. Also, I remain in constant touch with my project mentor with frequent communication every week.