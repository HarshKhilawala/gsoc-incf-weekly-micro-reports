.. _week2:


Week 2 Microreport (June 14 - June 20):
=======================================

What did I do this week?
-------------------------
Tried different set of experimental data to get idea on the output of the CerebUnit Validation tests. 

What are the plans for next week?
--------------------------------------
To iterate on series of data against the validation tests and comparing them by mean/median with final output.

Am I blocked on anything?
--------------------------
No