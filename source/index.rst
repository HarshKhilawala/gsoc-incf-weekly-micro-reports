.. GSoC INCF Weekly Microreports documentation master file, created by
   sphinx-quickstart on Fri Jun 11 11:01:15 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GSoC 2021 @ INCF
=================

Project: **Measure the Quality of CerebUnit Validation Tests**

Hi!👋🏻, I would begin with Thanking You🤗 for your time to read about my stuff for **GSoC 2021 @ INCF**.
Google Summer of Code (GSoC) is a great opportunity for students to meet with professionals from diverse communities and backgrounds with a chance to work and flourish under their guidance as a mentee. 

.. figure:: /images/GSoC@INCF.png
   :alt: GSoC Image
   :scale: 50%
   :align: center

   *Fig. Google Summer of Code*

I am **Harsh Khilawala**, Sophomore at *Charotar University of Science and Technology (CHARUSAT), India*. I would be contributing on the Project: **Measure the Quality of CerebUnit's Validation Tests**.
Also have great interest in the field of **Research** in the domains not limited to *AI/ML, Neural Networks, Web Semantics*. 


.. toctree::
   :maxdepth: 2
   :caption: Weekly Microreports:

   week1
   week2
   week3



