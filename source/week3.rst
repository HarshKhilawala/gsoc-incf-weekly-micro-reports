.. _week3:


Week 3 Microreport (June 21 - June 27):
=======================================

What did I do this week?
-------------------------
Try to generate bunch of json files for random data samples to work upon.

What are the plans for next week?
--------------------------------------
Observe and analyse the output of the validation test against the randomly generated data.

Am I blocked on anything?
--------------------------
No, But this week, I did get involved in wild goose hunt along with the mentor fixing potential bugs🐛!